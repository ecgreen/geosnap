//
//  TabBarController.swift
//  GeoSnap
//
//  Created by Ethan Green on 10/18/15.
//  Copyright © 2015 Ethan Green. All rights reserved.
//

import UIKit



class TabbarController: UITabBarController {
    var button : UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()

        //Selected Text Tab Color
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 0.05, green: 0.65, blue: 0.827, alpha: 1.0)], forState:.Selected)
        //Selected Icon Tab Color
        UITabBar.appearance().tintColor=UIColor(red: 0.25, green: 0.73, blue: 0.827, alpha: 1.0);
        
        let buttonImage = UIImage(named: "exploreToolbar")
        let highlightImage = UIImage(named: "exploreToolbarSelected")
        button = UIButton(type: UIButtonType.Custom) as UIButton
        
        button!.frame = CGRectMake(0, 0, buttonImage!.size.width, buttonImage!.size.height)
        button!.setBackgroundImage(buttonImage, forState: UIControlState.Normal)
        button!.setBackgroundImage(highlightImage, forState: UIControlState.Highlighted)
        button!.setBackgroundImage(highlightImage, forState: UIControlState.Selected)
        
        var center: CGPoint = self.tabBar.center
        center.y = center.y - 6.0
        //            center.y = center.y - heightDifference/2.0
        button!.center = center
        
        button!.addTarget(self, action: "centerTab:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button!)
        self.selectedIndex = 2
        button!.selected = true
        
    }
    
    func centerTab(sender: UIButton!) {
        self.selectedIndex = 2
        button!.selected = true
        
    }
    
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if(item != self.tabBar.items![2]){
            button!.selected = false
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
