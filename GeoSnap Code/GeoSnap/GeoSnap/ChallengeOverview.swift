//
//  ChallengeOverview.swift
//  GeoSnap
//
//  Created by Ethan Green on 1/15/16.
//  Copyright © 2016 Ethan Green. All rights reserved.
//

import UIKit


class ChallengeOverview : UIViewController {
    @IBOutlet var challengeRating: UIImageView!
    @IBOutlet var ratingCount: UILabel!
    @IBOutlet var difficulty: UIImageView!
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var viewButton: UIButton!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var descriptionField: UILabel!

    @IBOutlet var creatorname: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.navigationBar.barTintColor = UIColor.greenColor()
        self.setNavBarColor(pinData[selectedIndex].difficulty!)
        self.difficulty.image = UIImage(named: "difficulty" + String(pinData[selectedIndex].difficulty!))
        self.title = pinData[selectedIndex].title
        self.bgImage.image = pinData[selectedIndex].bgImg
        self.descriptionField.text = pinData[selectedIndex].challengeDescription
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        if(!tabBarIsVisible()){
            setTabBarVisible(true, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func zoomSelected(sender: AnyObject) {
        bgImage.layer.zPosition = 1000
        shareButton.enabled = false
        startButton.enabled = false
        navigationController?.setNavigationBarHidden(true, animated: true)
        setTabBarVisible(false, animated: true)
        
    }
    
    override func viewWillDisappear(animated : Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController()){ // turn the color of the nav bar back to gray
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 64/255, green: 64/255, blue: 64/255, alpha: 1)
        }
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(bgImage.layer.zPosition == 1000){
            if(setTabBarVisible(true, animated: true)){
                bgImage.layer.zPosition = -1
                shareButton.enabled = true
                startButton.enabled = true
                navigationController?.setNavigationBarHidden(false, animated: true)
            }
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBarController?.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame)
    }

    
    func setTabBarVisible(visible:Bool, animated:Bool) -> Bool {
        if (tabBarIsVisible() == visible) { return false}
    
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
    
        // zero duration means no animation
        let duration:NSTimeInterval = (animated ? 0.3 : 0.0)
    
            //  animate the tabBar
            if frame != nil {
                UIView.animateWithDuration(duration) {
                    self.tabBarController?.tabBar.frame = CGRectOffset(frame!, 0, offsetY!)
                    return
            }
        }
        return true
    }
    

    
    func setNavBarColor(difficulty: Int){
        switch(difficulty){
        case 1:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
            
        case 2:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 168/255, green: 212/255, blue: 84/255, alpha: 1)
            
        case 3:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 242/255, green: 222/255, blue: 0/255, alpha: 1)
            
        case 4:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 254/255, green: 144/255, blue: 64/255, alpha: 1)
            
        case 5:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 255/255, green: 91/255, blue: 60/255, alpha: 1)
            
        default:
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
        }
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "ChallengeDirections"){
            // Hide the bottom bar item
            setTabBarVisible(false, animated: true)
            
        } else {
            let backItem = UIBarButtonItem()
            backItem.title = "Explore"
            navigationItem.backBarButtonItem = backItem
        }
    }
}
