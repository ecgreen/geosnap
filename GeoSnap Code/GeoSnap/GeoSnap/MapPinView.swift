//
//  MapPinView.swift
//  GeoSnap
//
//  Created by Ethan Green on 1/3/16.
//  Copyright © 2016 Ethan Green. All rights reserved.
//

import Foundation
import MapKit

extension ExploreViewController: MKMapViewDelegate {
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapPin {
            let identifier = "pin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier){
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: 0, y: 6)
                
                let startButton = UIButton(type: .Custom)
                
                if let image = UIImage(named: "PlayButton") {
                    startButton.setImage(image, forState: .Normal)
                    startButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                }
                view.rightCalloutAccessoryView = startButton
                
                // Set the image of the annotation based on difficulty
                switch(annotation.difficulty!){
                case 1:
                    view.image = UIImage(named:"easyPin")
                    
                case 2:
                    view.image = UIImage(named:"normalPin")
                    
                case 3:
                    view.image = UIImage(named:"mediumPin")
                    
                case 4:
                    view.image = UIImage(named:"hardPin")
                    
                case 5:
                    view.image = UIImage(named:"veryHardPin")
                    
                default:
                    view.image = UIImage(named:"easyPin")
                }
            
            }
            view.layer.zPosition = -1
            return view
        }
        return nil
    }

    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        selectedIndex = (view.annotation as! MapPin).index
        performSegueWithIdentifier("showChallenge", sender: nil)
        
        
        }
    
}