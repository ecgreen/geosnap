//
//  FirstViewController.swift
//  GeoSnap
//
//  Created by Ethan Green on 10/18/15.
//  Copyright © 2015 Ethan Green. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

var selectedIndex : Int = 0

class ExploreViewController: UIViewController, CLLocationManagerDelegate  {
    let regionRadius: CLLocationDistance = 1000
    var locUpdated = false
    
    
    let locManager = CLLocationManager()
    
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locManager.requestWhenInUseAuthorization()
        locManager.requestAlwaysAuthorization()
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
        locManager.delegate = self
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Arial-BoldMT", size: 22)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(!locUpdated){
            centerMapOnLocation(locManager.location!)
            locUpdated = true
            getChallenges()
        }
    }
    
    func getChallenges(){
         pinData.removeAll() // Clear pre-stored data
        let request = NSMutableURLRequest(URL: NSURL(string: "http://localhost:8888/geosnap/php/getChallenges.php")!)
        request.HTTPMethod = "POST"
        
        var postString = "radius=" + String(10)
        postString += "&longitude=" + String(locManager.location!.coordinate.longitude)
        postString += "&latitude=" + String(locManager.location!.coordinate.latitude)
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        
        var result = "N/A"
        var message = 0
        
        let getChallengesTask = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            if error != nil {
                print("error=\(error)")
                return
            }
            
            do {
                let responseObject = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                result = responseObject["R"] as! String
                message = responseObject["M"] as! Int
                

                
                if(result == "S"){
                    for var i = 1; i <= message; ++i {
                        let name = responseObject["name" + String(i)] as! String
                        let longitude = responseObject["longitude" + String(i)] as! Double
                        let latitude = responseObject["latitude" + String(i)] as! Double
                        let difficulty = responseObject["difficulty" + String(i)] as! Int
                        
                        let decodedData = NSData(base64EncodedString: responseObject["img"+String(i)] as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
                        let bgImg = UIImage(data: decodedData!)
                        
                        let description = responseObject["description" + String(i)] as! String
                        let creator = responseObject["creator" + String(i)] as! Int
                        let creationTime = responseObject["creationTime" + String(i)] as! String
                        
                        print(String(longitude) + " , " + String(latitude))
                        let longCL = CLLocationDegrees(longitude)
                        let latCL = CLLocationDegrees(latitude)
                        
                        let coord = CLLocationCoordinate2D(latitude: latCL, longitude: longCL)
                        
                        let mapPin = MapPin(name: name, difficulty:  difficulty, coordinate: coord, bgImg: bgImg!, description: description, creator: creator, creationDateTime: creationTime)
                        pinData.append(mapPin)
                        
                        self.mapView.addAnnotation(mapPin)
                    }
                } else {
                    pinData.removeAll()
                }
            }
                
            catch{
                print(error)
            }
        }
        getChallengesTask.resume()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier! == "listView") {
            let backItem = UIBarButtonItem()
            backItem.title = "Map"
            navigationItem.backBarButtonItem = backItem
        
        } else { // Going to the ChallengeOverview page, so send data on the selected map pin
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
    }
}