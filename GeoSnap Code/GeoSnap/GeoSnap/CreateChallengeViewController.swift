//
//  SecondViewController.swift
//  GeoSnap
//
//  Created by Ethan Green on 10/18/15.
//  Copyright © 2015 Ethan Green. All rights reserved.
//

import UIKit
import Foundation

class CreateChallengeViewController: UIViewController {
    var selectedIndex = 0
    
    @IBOutlet var TitleField: UITextField!
    
    @IBOutlet var descriptionField: UITextView!
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var colorView: UIView!
    
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        // Check if all of the fields are correctly filled out
        if(TitleField.text != "" && descriptionField.text != "") {
            self.performSegueWithIdentifier("createTakePhoto", sender: self)
        } else {
            // Notify them that they must fill out the fields.
            let alertController = UIAlertController(title: "Fields Not Complete", message:
                "You must fill out the title and description fields to proceed.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func difficultySliderChanged(sender: UISlider) {
        let roundedVal = round(sender.value)
        sender.setValue(roundedVal, animated: true)
        difficultyChanged(Int(roundedVal))
        
    }
    
    @IBAction func veryEasyPressed(sender: UIButton) {
        difficultyChanged(0)
        
    }
    
    @IBAction func easyPressed(sender: UIButton) {
        difficultyChanged(1)
    }
    
    @IBAction func mediumPressed(sender: AnyObject) {
        difficultyChanged(2)
    }
    
    @IBAction func hardPressed(sender: AnyObject) {
        difficultyChanged(3)
    }
    
    @IBAction func veryHardPressed(sender: AnyObject) {
        difficultyChanged(4)
    }
    
    
    func difficultyChanged(index: Int) {
        selectedIndex = index
        
        switch(index){
            
        case 0:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
            })
            
        case 1:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 168/255, green: 212/255, blue: 84/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 168/255, green: 212/255, blue: 84/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 168/255, green: 212/255, blue: 84/255, alpha: 1)
            })
            
        case 2:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 242/255, green: 222/255, blue: 0/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 242/255, green: 222/255, blue: 0/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 242/255, green: 222/255, blue: 0/255, alpha: 1)
            })
            
        case 3:
              UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 254/255, green: 144/255, blue: 64/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 254/255, green: 144/255, blue: 64/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 254/255, green: 144/255, blue: 64/255, alpha: 1)
            })
            
        case 4:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 255/255, green: 91/255, blue: 60/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 91/255, blue: 60/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 91/255, blue: 60/255, alpha: 1)
                })
            
        default:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
                self.nextButton.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
                self.colorView.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
                
            })
        }
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionField.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).CGColor
        descriptionField.layer.borderWidth = 1.0
        descriptionField.layer.cornerRadius = 5
        
        nextButton.layer.cornerRadius = 5
        nextButton.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
        
        colorView.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
        
        navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 52/255, green: 207/255, blue: 0/255, alpha: 1)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)

    }
    
    override func viewWillDisappear(animated : Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController()){ // turn the color of the nav bar back to gray
            navigationController!.navigationBar.barTintColor = UIColor(colorLiteralRed: 64/255, green: 64/255, blue: 64/255, alpha: 1)
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

