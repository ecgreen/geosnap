//
//  NavigationBarController.swift
//  GeoSnap
//
//  Created by Ethan Green on 10/18/15.
//  Copyright © 2015 Ethan Green. All rights reserved.
//


import Foundation
import UIKit

class NavbarController : UINavigationController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Arial-BoldMT", size: 22)!, NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Arial-BoldMT", size: 20)!], forState: UIControlState.Normal)
        
        for parent in self.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor();
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
}
