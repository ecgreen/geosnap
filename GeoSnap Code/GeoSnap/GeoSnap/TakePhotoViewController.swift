//
//  takePhotoViewController.swift
//  GeoSnap
//
//  Created by Ethan Green on 3/13/16.
//  Copyright © 2016 Ethan Green. All rights reserved.
//

import UIKit
import MobileCoreServices

class TakePhotoViewController : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet var ImageView: UIImageView!
    var imagePicker : UIImagePickerController!
    
    @IBOutlet var takePhoto: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func takePhoto(sender: UIButton) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        ImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        
    }
    
}
