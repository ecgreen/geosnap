//
//  customMapPin.swift
//  GeoSnap
//
//  Created by Ethan Green on 1/3/16.
//  Copyright © 2016 Ethan Green. All rights reserved.
//

import Foundation
import MapKit

class MapPin: NSObject, MKAnnotation {
    let title: String?
    let difficulty: Int?
    let coordinate: CLLocationCoordinate2D
    let bgImg: UIImage
    let challengeDescription: String
    let creator: Int
    let creationDateTime: String
    let index : Int
    
    
    init(name: String, difficulty: Int, coordinate: CLLocationCoordinate2D, bgImg: UIImage, description: String, creator: Int, creationDateTime: String) {
        self.title = name
        self.difficulty = difficulty
        self.coordinate = coordinate
        self.bgImg = bgImg
        self.challengeDescription = description
        self.creator = creator
        self.creationDateTime = creationDateTime
        self.index = pinData.count
        super.init()
    }

    var subtitle: String? {
        switch(self.difficulty!){
        case 1:
           return "Difficulty: Easy"
            
        case 2:
            return "Difficulty: Normal"
            
        case 3:
            return "Difficulty: Medium"
            
        case 4:
            return "Difficulty: Hard"
            
        case 5:
            return "Difficulty: Very Hard"
            
        default:
            return "Difficulty: Easy"
        }
    }
}