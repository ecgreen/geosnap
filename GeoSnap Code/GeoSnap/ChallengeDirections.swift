//
//  ChallengeDirections.swift
//  GeoSnap
//
//  Created by Ethan Green on 1/18/16.
//  Copyright © 2016 Ethan Green. All rights reserved.
//

/* http://stackoverflow.com/questions/7173925/how-to-rotate-a-direction-arrow-to-particular-location */

import Foundation
import UIKit
import CoreLocation
import CoreMotion


class ChallengeDirections : UIViewController, CLLocationManagerDelegate {
    let locManager = CLLocationManager()
    let manager = CMMotionManager()
    var destLoc : CLLocationCoordinate2D!
    let distanceThreshold = 10

    @IBOutlet var levelLabel: UILabel!
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var compass: UIImageView!
    @IBOutlet var needle: UIImageView!
    @IBOutlet var distanceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        destLoc = pinData[selectedIndex].coordinate
        self.title = pinData[selectedIndex].title
        self.bgImage.image = pinData[selectedIndex].bgImg
        navigationItem.setHidesBackButton(true, animated: true)
        
        manager.gyroUpdateInterval = 1
        manager.startGyroUpdates()
       
        locManager.requestWhenInUseAuthorization()
        locManager.requestAlwaysAuthorization()
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
        locManager.delegate = self
        
        if manager.accelerometerAvailable {
            manager.accelerometerUpdateInterval = 0.5
            manager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: {(data : CMAccelerometerData?, error : NSError?) in
                
               if(data!.acceleration.z > -0.1 && data!.acceleration.z < 0.1){
                
                    UIView.animateWithDuration(1.0, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                            self.levelLabel.alpha = 0.0
                    }, completion: nil)

            
                } else {
                    if(self.levelLabel.alpha == 0.0 ){
                            UIView.animateWithDuration(1.0, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                            self.levelLabel.alpha = 1.0
                            }, completion: nil)

                    }
                }
        })
        }
    }
    
    

    @IBAction func quitSelected(sender: AnyObject) {
        setTabBarVisible(true, animated: true)
        navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
       let curLoc = newHeading.magneticHeading
       let bearingToLocation = bearingToLocationRadian(destLoc, currentLocation: locManager.location!)
        
        let transformCompass = CGAffineTransformMakeRotation(CGFloat(curLoc))
        compass.transform = transformCompass
        
        // rotate the needle
        let transformNeedle = CGAffineTransformMakeRotation(CGFloat(bearingToLocation))
        needle.transform = transformNeedle
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Update the distance label
        let mostRecentLoc = locations.last!
        let destination = CLLocation(latitude: destLoc.latitude, longitude: destLoc.longitude)
        
        let metersToFeet = 3.28084
        let distance = mostRecentLoc.distanceFromLocation(destination)
        let distanceFeet = Int(round(metersToFeet * distance))
        
        distanceLabel.text = String(distanceFeet) + " Feet"
        
        if(distanceFeet < distanceThreshold){
             self.performSegueWithIdentifier("cameraView", sender: self)
        }
        
    }
    
    
    func DegreesToRadians(degrees: Double ) -> Double {
        return degrees * M_PI / 180
    }
    
    func RadiansToDegrees(radians: Double) -> Double {
        return radians * 180 / M_PI
    }
    
    
    func bearingToLocationRadian(destinationLocation:CLLocationCoordinate2D, currentLocation: CLLocation) -> Double {
        
        let lat1 = DegreesToRadians(currentLocation.coordinate.latitude)
        let lon1 = DegreesToRadians(currentLocation.coordinate.longitude)
        
        let lat2 = DegreesToRadians(destinationLocation.latitude);
        let lon2 = DegreesToRadians(destinationLocation.longitude);
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        let radiansBearing = atan2(y, x)
        
        return radiansBearing
    }
    
    
    func tabBarIsVisible() ->Bool {
        return self.tabBarController?.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame)
    }
    
    
    func setTabBarVisible(visible:Bool, animated:Bool) -> Bool {
        if (tabBarIsVisible() == visible) { return false}
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration:NSTimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animateWithDuration(duration) {
                self.tabBarController?.tabBar.frame = CGRectOffset(frame!, 0, offsetY!)
                return
            }
        }
        return true
    }

}