<?php
require 'JWT.php';
require 'predis/autoload.php';

function getRandomString($length)
{
	$valid_chars = 'abcdefghijklmnopqrstuvwxyz';
	$num_valid_chars = strlen($valid_chars);
    
	$random_string = "";	    

	// repeat the steps until we've created a string of the right length
	for ($i = 0; $i < $length; $i++)
	{
		$random_pick = mt_rand(1, $num_valid_chars);
		$random_char = $valid_chars[$random_pick-1];
		$random_string .= $random_char;
	}
	return $random_string;
}

date_default_timezone_set('America/Los_Angeles');
$host="localhost"; // Host name 
$username="xdyvlt1479uuxvi"; // Mysql username 
$password="itf5423197778416729"; // Mysql password 
$db_name="geosnapp"; // Database name 
$serverTokenKey="1748asdfnowmnqokndsaoin1282k";

// Connect to the database
$mysqli = new mysqli("$host", "$username", "$password", $db_name); 
if ($mysqli->connect_error) {
	$result = array(); 
	$result["R"] 		= "F";
	$result["M"]		= "Could not connect to database.";
	echo json_encode($result);
} else {

	$userName 		= $_POST['user_name'];
	$pass	 		= $_POST['password'];

	// User Validation Block
	$passwordSalt = $mysqli->prepare("SELECT user_pass FROM Users WHERE user_name=?");
	if($passwordSalt === false) {
		$result = array(); 
		$result["R"] 		= "F";
		$result["M"]		= "Bad database query.";
		echo json_encode($result);
	} else if(empty($user_name) || empty($pass)){
		$result = array(); 
		$result["R"] 		= "F";
		$result["M"]		= "No User Name or Password.";
		echo json_encode($result);
	} else {
		$passwordSalt->bind_param("i", $user_name);
		$passwordSalt->execute();
		$passwordSalt->store_result();
		$passwordSalt->bind_result($encryptedPass);
		$passwordSalt->fetch();
	
		$salt = substr($encryptedPass, -4);
		$passwordSalt->close();
	
		// Hash the password
		$hashedPassword = hash('sha512', $pass . $salt).$salt;
	
		$validate = $mysqli->prepare("SELECT user_id FROM Users WHERE user_name=? AND user_pass=?;");
		$validate->bind_param('ss', $userName, $hashedPassword);
		$validate->execute();
		$validate->store_result();
		$validate->bind_result($userID);
		$validate->fetch();
	
		if($validate->num_rows == 1){ // Login was successful
			$tokin = [
				'iat'  => date_default_timezone_get(),   // Issued at: time when the token was generated
				'jti'  => getRandomString(10),          	// Json Token Id: an unique identifier for the token
				'iss'  => "i2dx",       					// Issuer
				'exp'  => "Never",           			// Expire
				'uid'  => $userID
			];
		   
			$result = array(); 
			$result["R"] 		= "S";
			$result["M"] 		= JWT::encode($tokin, $serverTokenKey);
			echo json_encode($result);
		
		} else { // Login was not successful
			$result = array(); 
			$result["R"] 		= "F";
			$result["M"] 		= "Invalid Patient ID or Password.";
			echo json_encode($result);
		}
	}
}
	
?>