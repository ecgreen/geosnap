<?php
require 'JWT.php';
require 'predis/autoload.php';

date_default_timezone_set('America/Los_Angeles');
$host="localhost"; // Host name 
$username="xdyvlt1479uuxvi"; // Mysql username 
$password="itf5423197778416729"; // Mysql password 
$db_name="geosnapp"; // Database name 
$tokenKey="1748asdfnowmnqokndsaoin1282k";

// Connect to the database
$mysqli = new mysqli("$host", "$username", "$password", $db_name); 
if ($mysqli->connect_error) {
	$result = array(); 
	$result["R"] 			= "F";
	$result["M"]			= "Could not connect to database.";
	echo json_encode($result);
	
} else {
	
	$challengeName 		= $_POST['challengeName'];
	$longitude	 		= $_POST['longitude'];
	$latitude 			= $_POST['latitude'];
	$difficulty			= $_POST['difficulty'];
	$description		= $_POST['description'];
	$encodedTokenID		= $_POST['tokenID'];
	
	$token = JWT::decode($encodedTokenID, $tokenKey);
	
	if(empty($token)) {
		$result = array(); 
		$result["R"] 		= "F";
		$result["M"]		= "Invalid Username or Password.";
		echo json_encode($result);
	} else {
		$user_id = $token->uid;
		$imgPath = '../Challenges/images/'.$user_id.'/'; 
		
		if (!file_exists($imgPath)) {
			mkdir($imgPath, 0777, true);
		}
		$imgLoc = $imgPath . "fftimg-" . date('Y-m-d-H:i:s') . ".png"; 
		move_uploaded_file($_FILES["img"]["tmp_name"], $imgLoc);
		
		
		$insertVals = $mysqli->prepare("INSERT INTO Challenges (challenge_name, longitude, latitude, difficulty, challenge_img, description, date_time, creator_id) VALUES (?,?,?,?,?,?,?)");
		$insertVals->bind_param('siiisssi', $challengeName, $longitude, $latitude, $difficulty, $imgLoc, $description, date("Y-m-d H:i:s"), $user_id);
		$insertVals->execute();
		$insertVals->close();
		
		Predis\Autoloader::register();
		try {
			$redis = new Predis\Client(); // get redis client to connect to server
			//$redis->incr('example');
		}catch (Exception $e) {
				die($e->getMessage());
		}
			
		$result = array(); 
		$result["R"] 		= "S";
		$result["M"]		= "A new challenge was created!";
		echo json_encode($result);
	}
}
?>