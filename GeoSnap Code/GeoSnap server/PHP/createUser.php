<?php
require 'JWT.php';
require 'predis/autoload.php';

function getRandomString($length)
{
	$valid_chars = 'abcdefghijklmnopqrstuvwxyz';
	$num_valid_chars = strlen($valid_chars);
    
	$random_string = "";	    

	// repeat the steps until we've created a string of the right length
	for ($i = 0; $i < $length; $i++)
	{
		$random_pick = mt_rand(1, $num_valid_chars);
		$random_char = $valid_chars[$random_pick-1];
		$random_string .= $random_char;
	}
	return $random_string;
}

date_default_timezone_set('America/Los_Angeles');
$host="localhost"; // Host name 
$username="xdyvlt1479uuxvi"; // Mysql username 
$password="itf5423197778416729"; // Mysql password 
$db_name="geosnapp"; // Database name 
$serverTokenKey="1748asdfnowmnqokndsaoin1282k";

// Connect to the database
$mysqli = new mysqli("$host", "$username", "$password", $db_name); 
if ($mysqli->connect_error) {
	$result = array(); 
	$result["R"] 		= "F";
	$result["M"]		= "Could not connect to database.";
	echo json_encode($result);
} else {
	
	$username		= $_POST['user_name'];
	$password 		= $_POST['password'];
	$email			= $_POST['email'];
	$salt			= getRandomString(4);

	// Connect to server and select database.
	if(empty($password) || empty($username) || empty($email)){
		$result = array(); 
		$result["R"] 			= "F";
		$result["M"]			= "No user name, password, or email.";
		echo json_encode($result);
	} else {	
		// Has the salt as the last four characters of the field
		$hashedPassword = hash('sha512', $password . $salt).$salt;
	
		$createUser = $mysqli->prepare("INSERT INTO Users (user_name, user_pass, user_email) VALUES (?, ?, ?)");
		$createUser->bind_param('sss', $userName, $hashedPassword, $email);
		$createUser->execute();

			
		$token = [
			'iat'  => date_default_timezone_get(),   	// Issued at: time when the token was generated
			'jti'  => getRandomString(10),          	// Json Token Id: an unique identifier for the token
			'iss'  => "i2dx",       					// Issuer
			'exp'  => "Never",           				// Expire
			'uid'  => $createUser->insert_id
		];
		$createUser->close();
		
		Predis\Autoloader::register();
		try {
			$redis = new Predis\Client(); // get redis client to connect to server
			//$redis->set('example', 0);
		}catch (Exception $e) {
			die($e->getMessage());
		}
	
		$result = array(); 
		$result["R"] 		= "S";
		$result["M"]     	= JWT::encode($token, $serverTokenKey);
		echo json_encode($result);
	} 
}
?>